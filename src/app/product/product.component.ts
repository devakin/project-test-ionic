import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { Product } from '../models/product';
import { ProductModalPage } from '../product-modal/product-modal.page';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
  providers: [ProductService],
})
export class ProductComponent implements OnInit {
  constructor(
    private productService: ProductService,
    private alertController: AlertController,
    private modalController: ModalController
  ) {}

  products: Product[];

  ngOnInit() {
    this.getProductsList();
  }

  getProductsList() {
    this.productService.getProductsList().subscribe((data) => {
      this.products = data;
    });
  }

  addProduct() {
    this.modalController
      .create({
        component: ProductModalPage,
      })
      .then((modal) => {
        modal.present();
        return modal.onDidDismiss();
      })
      .then(({ data, role }) => {
        if (role === 'created') {
          this.products.push(data);
        }
      });
  }

  updateProduct(product: Product) {
    this.modalController
      .create({
        component: ProductModalPage,
        componentProps: { product },
      })
      .then((modal) => {
        modal.present();
        return modal.onDidDismiss();
      })
      .then(({ data, role }) => {
        this.products = this.products.filter((product) => {
          if (data && data.id === product.id) {
            return data;
          }
          return product;
        });
      });
  }

  deleteProduct(productId) {
    this.alertController
      .create({
        header: 'Delete',
        message: 'Are you sure you want to delete?',
        buttons: [
          {
            text: 'Yes',
            handler: () => {
              this.productService.deleteProductById(productId).subscribe(() => {
                this.products = this.products.filter(
                  (product) => product.id !== productId
                );
              });
            },
          },
          { text: 'No' },
        ],
      })
      .then((alertEl) => alertEl.present());
  }
}
