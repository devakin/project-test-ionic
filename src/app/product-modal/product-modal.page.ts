import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { Product } from '../models/product';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-product-modal',
  templateUrl: './product-modal.page.html',
  styleUrls: ['./product-modal.page.scss'],
})
export class ProductModalPage implements OnInit {
  @Input() product: Product;
  isUpdate = false;
  data = {
    name: '',
    sku: '',
    ean13: 0,
    asin: '',
    isbn: '',
    price: 0,
    stock: 0,
  };

  constructor(
    private modalControler: ModalController,
    private productService: ProductService
  ) {}

  ngOnInit() {
    if (this.product) {
      this.isUpdate = true;
      this.data = this.product;
    }
  }

  closeModal() {
    this.modalControler.dismiss(null, 'closed');
  }

  onSubmit(form: NgForm) {
    const product = form.value;
    if (this.isUpdate) {
      this.productService
        .updateProduct(product, this.product.id)
        .subscribe(() => {
          this.modalControler.dismiss(product, 'update');
        });
    } else {
      this.productService.createProduct(product).subscribe(() => {
        this.modalControler.dismiss(product, 'created');
      });
    }
  }
}
