export class Product {
    constructor(public userId: number = null,
        public id: number = null,
        public name: string = null,
        public sku: string = null,
        public ean13: number = null,
        public asin: string = null,
        public isbn: string = null,
        public price: number = null,
        public stock: number = null
        ) {
    }
}