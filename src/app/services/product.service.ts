import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  path = 'https://blooming-taiga-20817.herokuapp.com/api/product/';
  

  constructor(private httpClient: HttpClient) {
    
  }

  getProductsList() {
    return this.httpClient.get<Product[]>(this.path);
  }

  getProductById(productId) {
    return this.httpClient.get<Product>(this.path + productId);
  }

  createProduct(product: Product) {
    return this.httpClient.post(this.path, product);
  }

  updateProduct(product: Product, productId) {
    return this.httpClient.put(this.path + productId, product);
  }

  deleteProductById(productId) {
    return this.httpClient.delete<void>(this.path + productId);
  }
}
