import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { Product } from '../models/product';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss'],
})
export class ProductDetailsComponent implements OnInit {
  constructor(
    private activatedRoute: ActivatedRoute,
    private productService: ProductService,
  ) {}

  product: Product;

  ngOnInit() {
    this.activatedRoute.params.subscribe((params) => {
      this.getProductById(params['productId']);
    });
  }

  getProductById(productId) {
    this.productService.getProductById(productId).subscribe((data) => {
      this.product = data;
    });
  }
}
